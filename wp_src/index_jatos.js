import 'jspsych';
import 'jspsych/css/jspsych.css'

if(typeof jatos !== 'undefined') {
    jatos.onLoad(function() {
        require('src_files/' + entry_js);
        let settings = jsPsych.initSettings();
        settings['on_finish'] = function() {
            let resultJson = jsPsych.data.get().json();
            jatos.submitResultData(resultJson, jatos.startNextComponent);
        };
    });
}
else
{
    require('src_files/' + entry_js);
}


